import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@bit/mui-org.material-ui.styles";
import Card from "@bit/mui-org.material-ui.card";
import CardActionArea from "@bit/mui-org.material-ui.card-action-area";
import CardActions from "@bit/mui-org.material-ui.card-actions";
import CardContent from "@bit/mui-org.material-ui.card-content";
import CardMedia from "@bit/mui-org.material-ui.card-media";
import Button from "@bit/mui-org.material-ui.button";
import Typography from "@bit/mui-org.material-ui.typography";
import NewsDialog from "./NewsDialog";
import { noImageFound } from "../utils/assets";

const styles = {
  card: {
    maxWidth: 300
  },
  media: {
    minHeight: 150
  }
};

function NewsCard({ article, classes }) {
  const [open, setOpen] = React.useState(false);
  const { title, urlToImage, author, publishedAt, description, url } = article;

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }
  return (
    <div>
      <Card className={classes.card}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={urlToImage ? urlToImage : noImageFound}
            title={title ? title : '💢no title found💢'}
            onClick={handleClickOpen}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {author ? author : '💢no author found💢'}
            </Typography>
            {` _ ${publishedAt.toString()}`}
            <Typography component="p">
              {description && description.substring(0, 100)}...
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary">
            <a href={url} target="blank">
              Learn More
            </a>
          </Button>
        </CardActions>
      </Card>
      <NewsDialog article={article} open={open} handleClose={handleClose} />
    </div>
  );
}

NewsCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(NewsCard);
