import React from "react";
import PropTypes from "prop-types";
import Button from "@bit/mui-org.material-ui.button";
import { withStyles } from "@bit/mui-org.material-ui.styles";
import SnackbarContent from "@bit/mui-org.material-ui.snackbar-content";

const action = (
  <Button color="secondary" size="small">
    lorem ipsum dolorem
  </Button>
);

const styles = theme => ({
  snackbar: {
    margin: theme.spacing.unit
  }
});

function LongTextSnackbar(props) {
  const { classes, message } = props;

  return (
    <div>
      <SnackbarContent
        className={classes.snackbar}
        message={message}
        action={action}
      />
    </div>
  );
}

LongTextSnackbar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(LongTextSnackbar);
