import React from "react";
import Button from "@bit/semantic-org.semantic-ui-react.button";
import StyleLinks from "@bit/semantic-org.semantic-ui-react.internal.style-links";
import PropTypes from "prop-types";

const FetchButton = ({ fetchDataThunk, fetchDataSaga }) => (
  <>
    <StyleLinks />
    <Button.Group>
      <Button onClick={() => fetchDataThunk("bitcoin")}>Fetch Bitcoin news with Rdux-Thunk</Button>
      <Button.Or />
      <Button onClick={() => fetchDataSaga("ethereum")}>Fetch Ethereum news with Rdux-saga</Button>
    </Button.Group>
  </>
);
FetchButton.propTypes = {
    fetchDataThunk: PropTypes.func,
    fetchDataSaga: PropTypes.func
};
export default FetchButton;
