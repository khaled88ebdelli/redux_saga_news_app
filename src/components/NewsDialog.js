import React from "react";
import Button from "@bit/mui-org.material-ui.button";
import Dialog from "@bit/mui-org.material-ui.dialog";
import DialogActions from "@bit/mui-org.material-ui.dialog-actions";
import DialogContent from "@bit/mui-org.material-ui.dialog-content";
//import DialogContentText from "@bit/mui-org.material-ui.dialog-content-text";
import DialogTitle from "@bit/mui-org.material-ui.dialog-title";
import NewsItem from "./NewsItem";

function NewsDialog({ article, open, handleClose }) {
  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title"> {article.title}</DialogTitle>
        <DialogContent>
          <NewsItem article={article} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            exit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default NewsDialog;
