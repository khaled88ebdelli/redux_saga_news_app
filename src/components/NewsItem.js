import React from "react";
import { noImageFound } from "../utils/assets";
import Button from "@bit/mui-org.material-ui.button";

const imgStyle = {
  hight: "auto",
  width: "50%",
  borderRadius: "5%",
  display: "inline-block"
};
const articleStyle = {
  width: "80%",
  margin: "0 auto",
  color: "olive",
  padding: "5px"
};
const NewsItem = ({ article }) =>
  article ? (
    <div style={articleStyle}>
      <div>
        <h1>{article.title}</h1>
        {article.urlToImage && (
          <img style={imgStyle} src={article.urlToImage} alt="" />
        )}
        {!article.urlToImage && (
          <img style={imgStyle} src={noImageFound} alt="not found" />
        )}
        <h4>{article.description}</h4>
        <Button size="small" color="info" href={article.url} target="blank">
          Source
        </Button>
      </div>
    </div>
  ) : null;
export default NewsItem;
