import axios from 'axios';

export default axios.create({
	baseURL: `https://newsapi.org/v2/everything?from=${new Date().getDate()}&sortBy=publishedAt&apiKey=52e50a90c6ee4cdbb2e78ab9fde5328b&q=`
});
