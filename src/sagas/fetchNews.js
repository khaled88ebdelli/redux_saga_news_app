import { takeLatest, put, call } from "redux-saga/effects";
import API from "../utils/API";
import {
  FETCH_NEWS_SAGA,
  FETCH_NEWS_START,
  FETCH_NEWS_SUCCESS,
  FETCH_NEWS_ERROR
} from "../actions/types/newsTypes";

//Every time we dispatch an action
//that has a type property "FETCH_NEWS_SAGA"
// call the fetchNewsSaga function
export default function* watchFetchNewsSaga() {
  // ☘️ takeLatest(pattern: String | Array | Function, saga: Function, ...args: Array<any>)
  // takeLatest cancels any pending task started previously,
  // we ensure that if a user triggers multiple consecutive FETCH_NEWS_SAGA actions rapidly,
  // we'll only conclude with the latest action (high-level API built using take and fork)
  // ☘️ takeLeading will only keep on running with the leading action (high-level API built using take and call)
  yield takeLatest(FETCH_NEWS_SAGA, fetchNewsSaga);
}

function* fetchNewsSaga({ subject }) {
  yield put({ type: FETCH_NEWS_START });
  // call([context, fnName], ...args)
  // Creates an Effect description that instructs the middleware to call the function error from console fn.
  yield call([console, 'error'], 'redux-saga')
  // wrap our code to catch errors if something went wrong
  try {
    // fetch data in async way and write it into newsResponse since payload is resolved (2)
    const newsResponse = yield API.get(`${subject}`);
    // parse JSON into object data
    const { data } = yield newsResponse;
    // since parse process is finished, dispatch FETCH_DATA_SUCCESS with payload
    yield put({ type: FETCH_NEWS_SUCCESS, payload: { data, subject } });
  } catch (error) {
    // if something goes wrong dispatch FETCH_NEWS_ERROR with error payload to handle it outside
    yield put({ type: FETCH_NEWS_ERROR, error });
  }
}
