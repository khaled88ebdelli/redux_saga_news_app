import {
  FETCH_NEWS_ERROR,
  FETCH_NEWS_START,
  FETCH_NEWS_SUCCESS
} from "./types/newsTypes";

export const fetchNewsError = (payload) => {
  return { type: FETCH_NEWS_ERROR, payload };
};

export const fetchNewsStarted = () => {
  return { type: FETCH_NEWS_START };
};

export const fetchNewsSuccess = payload => {
  return { type: FETCH_NEWS_SUCCESS, payload };
};
